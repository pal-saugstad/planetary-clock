
# Split Ring Planetary gear

Originally, this was a description and an animation of a clock mechanism where planetary gears are used to
set the correct radio reductions between the hands.

However, later I learned about the concept "Split Ring Planetary Gear" and understood that my clock animation
and Split Ring Planetary gears are almost the same!
Hence, the current animation is optimized to show layouts of Split Ring Planetary gears.

The animation can be found [here](https://saugstad.net/gear-animation).
A pure clock animation can be found [here](https://saugstad.net/gear-animation/clock.html).
The planetary gears are based on the work by [Ryan Cahoon](http://www.thecatalystis.com/) found [here](http://www.thecatalystis.com/gears/).
My modified source code for this animation can be found in this repository in the 'animation' folder.

These web pages are written in html/css/javascript, and all the functionality is thus available from a Browser.


## Initial inspiration

I was inspired by the planetary gear clock of William Strutt (early 1800):

- [Clayton Boyer](https://lisaboyer.com/Claytonsite/epicyclicpage1.htm): Photo of his William Strutt clock.
- [Ken Kuo](https://youtu.be/uwv7Z2I-JiI): Animation video of the the inner workings of the William Strutt clock.

The William Strutt clock looks great, but my idea was to use 'simple' planetary gears, where the same gear (or three gears spread around in the planet gap)
is connected both to the Output gear (Sun gear) and the Input gear (Ring gear).


## Differences between Strutt's design and mine

In Strutt's clock, the Minute hand is fixed to the planet carrier.
The Hour hand is connected to the Sun gear in front.
This Sun gear is manipulated by the planet gear since there are two Sun gears with almost the same number of teeth,
and the back Sun gear is fixed.

Let's name the base gear, output gear, planet carrier and input gear B, O, C and I.

- B (base) is the fixed gear.
- O is the output gear (moving Ring (or Sun) gear).
- C is the Planet carrier
- I is the Input gear

In Strutt's design, O has 1 revolution per 12 hours and C has 12 revolutions per 12 hours.

In my design B and O have the same function.
However, the planet carrier C is not connected to anything.
Instead, the minute hand is connected to the Input gear, I (Ring gear).
In Strutt's design, the planet gear which is connected to the Ring gear is really big.
In my design, the planet gear components are almost equal in dimensions.

The advantage of the big planet gear in Strutt's design is that the Input gear moves really fast.
In my design the Ring gear moves one revolution per hour.


## Main principles of my design

The Split Ring Planetary Gear Generator calculates alternatives for I, O and B which gives the desired ratio.
Since C isn't connected to any hands of the clock, it isn't restricted to run in a particular speed.

The main output from the program is as follows:

```
          No of teeth   |      Ratios      |  Approx. Ratios  |
   #:     I     O     B |    i : c : o     |    i : c : o     |

   1:    14     8     7 |    12     8    1 |     12    8.00 1 |
```

Here the input has 14 teeth, the output 8 teeth and the fixed base has 7 teeth.
The revolutions from this gear is shown next.
The ratios i:c:o (input:carrier:output) is 12:8:1.
So, the Output (Sun) can be used for the Hour hand and the Input (Ring) for the Minute hand.
The Carrier revolves 8 times in 12 hours.

Of course, the number of teeth might be multiplied by a factor without changing the functionality.
In fact, multiplying by three has the great advantage that there will be three planet positions 120 deg. apart
which will be identical, so that three identical planets will fit perfectly.

However, it **is** possible to place three planets regardless of the exact number of teeth.
They might not be located exactly 120 deg. apart, though.
The number of positions a planet can have if we remove `O`, is `(B + I)` or remove `B` is `(O + I)`.

First, check if `(I-B)/2` or `(I-O)/2` happens to be exactly the number of teeth chosen for the planet, P.
If one of them are, one of the two planet gear radius will fit into both I and the O or B.

Then remove the 'extra' gear (O or B).
In the above example, `B` must be removed if `P = 3`.
The number of planet positions is therefore `14+8` = `22`.
For the planet position, we might choose position 0, 7 and 14, meaning that there will be 1 step more (8) between
two of the planets. in other words, almost 120 deg., but not quite.

For those three planets,
the extra gear with slightly different radius which will fit into `B` can't be exactly identical for all three planets.
The extra gear must be rotated to a slightly different angle for the three chosen positions of the planets to fit perfectly.

The number of teeth of the planet gear can be chosen sort of freely without any impact of the gear ratios, as long
as the same number of teeth are used for all three connections.
However, the number of teeth will have an impact on the radii of the different gears.

## Gear ratio calculations

The calculations I use are similar to this explanation: https://youtu.be/9c1CyklAN5A

I, O and B are the number of teeth of the Input, Output and Base gear.
A 'Help variable' I+O is the virtual number of teeth of planet carrier C which makes calculations simple.

Rotation speeds: Let i, c and o be the RPMs (or Rotations per 12 Hours, rather) of Input, Planet carrier and Output.
Please note that the Planet carrier is the frame where the planet gears are mounted.
Hence, c represents revolutions of the planet carrier.

### Calculate i, o and c

We can set i to the ratio (r) and o to 1 since the ratio is i:o or i/o

Calculating c, the carrier speed is straight forward since `b = 0`:
```
a) c = i * I / (I+B) = r * I / (I+B)
```
where capital letters are number of teeth and lower case letters are revolutions.

So, for example, if I = 12 and B = 6, then `c = r * 2/3`, meaning that I must turn 3 times for the carrier to turn 2 times.

Calculating c with respect to O and B is a variant of formula a) since both gears are connected to the same side of the planets (hence the minus sign):
```
b) c = o * O / (O-B) = O / (O-B)
```
Taking `c` out and combining a) and b) gives:
```
c) r * I / (I+B) = O / (O-B)
```
This can be rearranged to
```
d) r * I * (O-B) = O * (I+B)
```

So, if we know (have guessed) O and B, we can calculate `c` using b). Then, we can use formula a) to find 'I'.
a) can be rewritten
```
c * (I+B) = r * I
c*I + c*B = r*I
I * (r-c) = c*B
e) I = c*B / (r-c) = B / (r/c - 1)
```
If 'I' isn't a whole number, the values we guessed for O and B wasn't a real solution. In the calculator, e) is used to find a possible value for 'I',
then d) is used to check if that really was a solution.


## The Split Ring Planetary Gear Calculator

The Split Ring Planetary Gear Calculator program outputs gear ratios in a table.
Input parameters to the program is the ratio between Input and Output and a limit value regarding the size of the Input.

In the Browser based view of the [calculator](https://saugstad.net/gear-animation/calculate.html), the parameters are explained when mousing over them,
and the input parameters and table output is explained within the calculators opening page, and when pressing 'Help'.
