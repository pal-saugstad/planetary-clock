
pi=Math.PI;

// gear parameter setup
axle_radius=20; // center hole radius in pixels
input_margin=20;
// Pressure_angle, determines gear shape, range is 10 to 40 degrees, most common is 20 degrees
pressure_angle = 30 * pi / 180 ; // convet 20 degrees to radians
input_thickness = 14;
input_overlap = 10;

browser_calculate = true;
