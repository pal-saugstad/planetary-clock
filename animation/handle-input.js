
function changeTick(elem) {
  document.getElementById("action").innerHTML = 1;
}

function changeSpeed(elem) {
  document.getElementById("action").innerHTML = 2;
}

function changeInput(elem, inc) {
  var parent_id = elem.parentNode.id;
  var dest = parent_id + 'T';
  var curr_val = parseInt(document.getElementById(dest).innerHTML);
  var val = curr_val + inc;
  var lowest = 1;
  var highest = 1000;
  var action = 1;
  switch (parent_id) {
    case "mult":
      lowest = 1;
      highest = 9;
    break;
    case "zoom":
      lowest = 1;
      highest = 10;
      action = 2;
    break;
    case "tilt":
      lowest = 0;
      highest = 20;
      action = 2;
    break;
    case "planets":
      lowest = 1;
      highest = 10;
      action = 1;
    break;
    case "speed":
      var pos_val = val;
      if (val < 0) pos_val = - val;
      if (inc > 1) {
        val += Math.floor(pos_val/10);
      }
      if (inc < -1) {
        val -= Math.floor(pos_val/10);
      }
      lowest = -100;
      highest = 100;
      action = 4;
    break;
  }
  if (val < lowest) val = lowest;
  if (val > highest) val = highest;
  if (val != curr_val) {
    document.getElementById(dest).innerHTML = val;
    document.getElementById("action").innerHTML = action;
  }
}

function useGears(params) {
  var numbers = params.match(/\d+/g);
  if (numbers.length == 3) {
    document.getElementById("inputT").innerHTML = numbers[0];
    document.getElementById("outputT").innerHTML = numbers[1];
    document.getElementById("baseT").innerHTML = numbers[2];
    document.getElementById("action").innerHTML = 1;
  }
}

function newGears(elem) {
  var params = elem.innerHTML;
  document.getElementById("inputvals").value = params;
  useGears(params);
}

function inputGears(elem) {
  useGears(elem.value);
}
