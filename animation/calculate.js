/*
Pal Saugstad, 2021-01-13 (pal@saugstad.net)

This javascript can be run as a standalone file from command line when using
a command line javascript interpreter (JavaScript shell)

In addition, this file can be part of a html/javascript web server installation.
If so, a global variable, 'browser_calculate' must in defined in anouther javascript file.
The effect of that is that the variable 'use_console' becomes false (see below).
*/

var use_console = (typeof(browser_calculate) === 'undefined');
var flush_text = "";
var texts = {};

function table_flush() {
  texts = {};
  if (use_console) {
    console.log(flush_text);
    flush_text = "---- Flushing ----";
  }
}

function table_print(sort, text) {
  texts[sort] = text;
  var out = '';
  for (idx in texts) {
    out += texts[idx] + "\n";
  }
  if (use_console) console.log(out);
  else document.getElementById("preview").innerHTML = out;
}

function factor(num) {
    var fnum = {};
    if (num < 0) num = -num;
    if (num <= 3) {
      fnum[num] = 1;
      return fnum;
    }
    fact = 2;
    while (2 * fact <= num) {
      while ((num % fact) == 0) {

        if (fnum[fact] !== undefined) {
          fnum[fact] += 1
        } else {
          fnum[fact] = 1
        }
        num /= fact;
      }
      fact++;
    }
    if (num > 1) {
      if (fnum[num] !== undefined) {
        fnum[num] += 1
      } else {
        fnum[num] = 1
      }
    }
    return fnum;
}

function reduce(a) {
  var f = [];
  for (var n in a) if (a[n] != 0) f.push(factor(a[n]));
  reduce_fact = 1;
  p = f.pop();
  for (var idx in p) {
    times = p[idx];
    for (var jdx in f) {
      q = f[jdx];
      if (q[idx] === undefined) {
        times = 0;
        break;
      }
      if (q[idx] < times) times = q[idx];
    }
    while (times) {
      reduce_fact *= idx;
      times--;
    }
  }
  for (var n in a) a[n] /= reduce_fact;
  return a;
}

function fmt_float(val) {
  return ' ' + val.toFixed(3).toString().padStart(7);
}

function report_line(sort, count, I, O, B, ratio_text) {

  Bd = B.toString().padStart(5);
  Od = O.toString().padStart(5);
  Id = I.toString().padStart(5);

  table_print(sort, count.toString().padStart(4) + ': ' + Id + ' ' + Od + ' ' +  Bd + ' | ' + ratio_text + ' |');
}


function table_heading() {
  heading = "" +
     "          No of teeth   |      Ratios      |  Approx. Ratios  |\n" +
     "   #:     I     O     B |    i : c : o     |    i : c : o     |\n";
  return heading;
}

var ratio_in = '0', input_max_dist = 2.0, input_min_dist = 0.0, count = 0, max_count = 0, abort = false, same_direction = false;
var B = 1;

function* find_gears_generator() {
  var tested = 0;
  while (true) {
    abort = false;
    var numbers = ratio_in.match(/[^:]+/g);
    if (numbers.length == 1) numbers.push(1);
    if (numbers[1] == 0) numbers[1] = 1;
    var mask = 0;
    if (numbers[0] < 0) {
      numbers[0] = -numbers[0];
      mask = 1; // Don't search for same direction solutions
    } else {
      var n = numbers[0].search("[\+]")
      if (n >= 0) mask = 2; // Don't search for opposite direction solutions
    }
    var ratio_arr = reduce(numbers);
    var ratio = ratio_arr[0] * 1.0 / ratio_arr[1];
    //table_flush();
    table_print(0, table_heading());
    if (ratio <= 1.0) {
      abort = true;
      yield -1;
      continue;
    }
    for (o_b_dist = 1; !abort; o_b_dist++) {
      work_done = mask; // work_done |= same_opposite_check to tell this case is done, so if work_done == 1, no need to check positive cases (when sign = 1)
      for (base = 1; !(abort || (work_done == 3)); base++) {
        for (same_opposite_check = 1; same_opposite_check < 3; same_opposite_check++) {
          if (same_opposite_check & work_done) continue;
          sign = same_opposite_check & 2 ? -1 : 1;
          if (sign > 0) {
            B = base;
            O = base + o_b_dist;
          } else {
            O = base;
            B = base + o_b_dist;
          }
          tested += 1;
          if (tested >= 10000) {
            max_count = count;
            yield count;
            tested = 0;
          }

          var c = 1.0 * O / o_b_dist;
          if (c >= ratio) {
            work_done |= same_opposite_check;
            continue;
          }
          var I = Math.round(1.0 * B / (ratio/c - 1.0));
          if (I > O * input_max_dist) {
            work_done |= same_opposite_check;
            continue;
          }
          sort = (c * 1000000).toFixed(0) * 2 + (sign < 0 ? 1 : 0);
          if (texts[sort] === undefined) {
            if (I > O * input_min_dist) {
              if (O * (I + B) * ratio_arr[1] == o_b_dist * I * ratio_arr[0]) {
                i_c_o = reduce ([ratio_arr[0] * o_b_dist, ratio_arr[1] * O, ratio_arr[1] * (O - B)])
                ratio_text = i_c_o[0].toString().padStart(5) + ' ' + i_c_o[1].toString().padStart(5) + ' ' + i_c_o[2].toString().padStart(4);
                r_ = (i_c_o[0] / i_c_o[2]).toFixed((i_c_o[0] % i_c_o[2] == 0) ? 0 : 2);
                c_ = (i_c_o[1] / i_c_o[2]).toFixed(2);
                ratio_text += ' | ' + r_.toString().padStart(6) + ' ' + c_.toString().padStart(7) + ' 1';
                count += 1;
                report_line(sort, count, I, O, B, ratio_text);
                yield count;
                tested = 0;
              }
            }
          }
        }
      }
    }
  }
}

const gears_it = find_gears_generator();

function calculate() {
      let result = gears_it.next();
      document.getElementById("found").innerHTML = result.value;
      if (abort) {
        document.getElementById("end").innerHTML = result.value == -1 ? "Can't make sense of input parameters" : "Aborted!";
        document.getElementById("calc").value = "Calculate!";
      } else if (result.value >= max_count) {
        document.getElementById("end").innerHTML = result.value + " solutions";
        document.getElementById("calc").value = "Continue!";
      } else {
        document.getElementById("end").innerHTML = result.value + " solutions ...";
        setTimeout("calculate()", 0);
      }
}

function pre_calculate() {
  if (document.getElementById("calc").value == "Working...") {
    max_count = count;
  } else {
    document.getElementById("calc").value = "Working...";
    ratio_in = document.getElementById("customRatio").value;
    max_count = max_count - max_count % 5 + 25;
    input_max_dist = parseFloat(document.getElementById("inputLimit").value);
    input_min_dist = 0.0;
    from_one = '';
    if (input_max_dist > 1.0) {
      if (document.getElementById("inputLimit").value.search("[\+]") >= 0) {
        input_min_dist = 0.99;
        from_one = '+';
      }
    }
    document.getElementById("inputLimit").value = from_one + input_max_dist;
    document.getElementById("customRatio").disabled = true;
    document.getElementById("inputLimit").disabled = true;
    document.getElementById("end").innerHTML = "...";
    setTimeout("calculate()", 1);
  }
}

function stop_calculate() {
  abort = true;
  if (document.getElementById("calc").value != "Working...") {
    max_count = 0;
    count = 0;
    document.getElementById("customRatio").disabled = false;
    document.getElementById("inputLimit").disabled = false;
    document.getElementById("calc").value = "Calculate!";
    table_flush();
  }
}

if (use_console) {
  p = ['0','1','100:1','.9', '5'];
  console.log("\nPlanetary Gear Calculator\n\nParameters:");
  console.log("  ratio input_limit iterations");
  console.log("Default values:");
  console.log("  " + p[2] + " " + p[3] + " " + p[4]);
  for (n in process.argv) p[n] = process.argv[n];
  console.log("Selected values: ");
  console.log("  " + p[2] + " " + p[3] + " " + p[4]);
  ratio_in = p[2];
  max_count = parseInt(p[4]);
  input_max_dist = parseInt(p[3]);
  if (max_count > 0) {
    let result = gears_it.next();
    max_count--;
    while (max_count > 0) {
      result = gears_it.next();
      max_count--;
    }
  } else {
    var assert = require('assert');
    console.log("Run function tests since the input of iterations was 0");
    console.log("Run function tests of factor( value )");
    // The result should be an object which is describing the factors of the number like shown below

    assert.deepEqual( factor(0),     {0: 1});
    assert.deepEqual( factor(1),     {1: 1});
    assert.deepEqual( factor(2),     {2: 1});
    assert.deepEqual( factor(3),     {3: 1});
    assert.deepEqual( factor(4),     {2: 2});
    assert.deepEqual( factor(5),     {5: 1});
    assert.deepEqual( factor(6),     {2: 1, 3: 1});
    assert.deepEqual( factor(75),    {3: 1, 5: 2});
    assert.deepEqual( factor(1024),  {2: 10});
    assert.deepEqual( factor(1001),  {7: 1, 11: 1, 13: 1});
    assert.deepEqual( factor(-1001), {7: 1, 11: 1, 13: 1});

    console.log("Run function tests of reduce( Array() )");
    // The result should be an Array where the array elements are the original values divided by the largest possible common factor

    assert.deepEqual( reduce([2, 4, 6, 8, 10, 12]), [1, 2, 3, 4, 5, 6]);
    assert.deepEqual( reduce([12, 4]), [3, 1]);
    assert.deepEqual( reduce([20, -5, 10]), [4, -1, 2]);
    assert.deepEqual( reduce([1001, 1024]), [1001, 1024]);
    assert.deepEqual( reduce([100, 0, 75]), [4, 0, 3]);
    assert.deepEqual( reduce([0, 0]), [0, 0]);
  }
}
