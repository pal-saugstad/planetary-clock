var Animation = function(){
    this.animating = false;

    // provided by Paul Irish
    window.requestAnimFrame = (function(callback){
        return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function(callback){
            window.setTimeout(callback, 1000 / 60);
        };
    })();
};

Animation.prototype.setStage = function(func){
    this.stage = func;
};

Animation.prototype.start = function(){
    this.animating = true;

    if (this.stage !== undefined) {
        this.animationLoop();
    }

};

Animation.prototype.stop = function(){
    this.animating = false;
};

Animation.prototype.animationLoop = function(){
    var that = this;

    this.stage();

    if (this.animating) {
        requestAnimFrame(function(){
            that.animationLoop();
        });
    }
};
