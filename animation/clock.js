
window.onload = function(){
  var numPlanets = 5;
  var toothMult = 1;
  var B = 3, O = 5, I = 7;
  var sec_B = 3, sec_O = 5, sec_I = 7;
  var sec_P = 1;
  var sec_inputSpeed = 120;
  var preferBase = false;
  var freeze = false;
  var tilt = 0;
  svg_side = 750;

  var P = 1;
  var size = 1;
  var sec_size = 1;
  var zoom = 1;
  var Z = 1;
  var realTime = false;

  var outputAngle = 0;
  var carrierAngle = 0;
  var inputAngle = 0;

  var base = new Gear(B, P, size, false, "rgba(180, 0, 0, 1.0)", 0);
  var output = new Gear(O, P, size, false, "rgba(180, 180, 0, 0.8)", 1, 190);
  var planetB = new Gear(P, O, size, false, "rgba(180, 0, 0, 1.0)", 0);
  var planetO = new Gear(P, B, size, false, "rgba(160, 140, 0, 1.0)", 0);
  var input = new Gear(I, P, size, true, "rgba(180, 0, 0, 1.00)", 0);
  var inputPointer = new Gear(I, P, size, true, "rgba(180, 180, 0, 1.0)", 2, 240);
  var sec_base = new Gear(sec_B, sec_P, sec_size, false, "rgba(180, 0, 0, 1.0)", 0);
  var sec_output = new Gear(sec_O, sec_P, sec_size, false, "rgba(160, 140, 0, 1.0)", 0);
  var sec_planetB = new Gear(sec_P, sec_O, sec_size, false, "rgba(180, 0, 0, 1.0)", 0);
  var sec_planetO = new Gear(sec_P, sec_B, sec_size, false, "rgba(160, 140, 0, 1.0)", 0);
  var sec_input = new Gear(sec_I, sec_P, sec_size, true, "rgba(160, 140, 0, 1.0)", 1, 255);

  var this_input = 0;
  var gear_idx = 0; // 0 -> Panetary gear is made with base (red) 1 -> Panetary gear is made with output (yellow)
  var anim = new Animation();

  var svg_image = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg_image.setAttribute("height", svg_side.toString());
  svg_image.setAttribute("width", svg_side.toString());
  svg_image.setAttribute("viewBox", -svg_side/2 + " " + -svg_side/2 + " " + svg_side + " " + svg_side);
  svg_image.setAttribute("preserveAspectRatio", "xMidYMid slice");

  var defs = document.createElementNS("http://www.w3.org/2000/svg", "defs");
  defs.appendChild(planetB.group);
  defs.appendChild(planetO.group);
  defs.appendChild(sec_planetB.group);
  defs.appendChild(sec_planetO.group);
  planetB.group.setAttribute("id", "planetB");
  planetO.group.setAttribute("id", "planetO");
  sec_planetB.group.setAttribute("id", "sec_planetB");
  sec_planetO.group.setAttribute("id", "sec_planetO");

  svg_image.appendChild(defs);
  svg_image.appendChild(input.group);
  svg_image.appendChild(base.group);
  svg_image.appendChild(sec_base.group);
  svg_image.appendChild(output.group);
  svg_image.appendChild(sec_output.group);
  svg_image.appendChild(inputPointer.group);
  svg_image.appendChild(sec_input.group);
  var planets = [];
  var carrierAnglePos = [];
  var planetAngelOffset = [];
  var sec_carrierAnglePos = [];
  var sec_planetAngelOffset = [];

  document.getElementById("preview").appendChild(svg_image);

  anim.setStage(function(){

    // Read input from html
    var calc_from_scratch = false;
    var action = parseInt(document.getElementById("action").innerHTML);
    updated = false;
    if (action != 0) {
      if (action & 1) calc_from_scratch = true;
      var inputs = document.getElementById("outerT").innerHTML.match(/\d+/g);
      if (inputs.length == 4) {
          sec_I += inputs[1] - sec_I;
          sec_O += inputs[2] - sec_O;
          sec_B += inputs[3] - sec_B;
      }
      toothMult = parseInt(document.getElementById("multT").innerHTML);
      var inputs = document.getElementById("innerT").innerHTML.match(/\d+/g);
      if (inputs.length == 4) {
          I = toothMult * inputs[1];
          O = toothMult * inputs[2];
          B = toothMult * inputs[3];
      }
      numPlanets = parseInt(document.getElementById("planetsT").innerHTML);
      realTime = true;
      sec_P = Math.floor((sec_I-sec_O)/2);
      Pb = Math.floor((I-B)/2);
      Ps = Math.floor((I-O)/2);
      P = Ps;
      s_calc = O;
      gear_idx = 1;
      if (preferBase) {
        P = Pb;
        s_calc = B;
        gear_idx = 0;
      }
      if (P == 0) {
        P = Pb;
        s_calc = B;
        gear_idx = 0;
      }
      sec_size = Math.floor((sec_P + sec_O) / (2 * sec_P + sec_O) * (svg_side/2 - input_thickness));
      inner_side = ((svg_side - 2*input_thickness) * sec_O / sec_I) - 2*input_thickness;
      size = Math.floor((P + s_calc) / (2 * P + s_calc) * (inner_side/2 - input_thickness + input_overlap));
      Z = size;
      svg_image.setAttribute("viewBox", -svg_side/2/zoom + " " + (-svg_side/2/zoom - tilt*svg_side/40) + " " + svg_side/zoom + " " + svg_side/zoom);
      document.getElementById("action").innerHTML = 0;

      if (planets.length < numPlanets*4) {
        while(planets.length < numPlanets*4) {
          var pB = document.createElementNS("http://www.w3.org/2000/svg", "use");
          pB.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#planetB");
          svg_image.insertBefore(pB, output.group);
          planets.push(pB);
          var pO = document.createElementNS("http://www.w3.org/2000/svg", "use");
          pO.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#planetO");
          svg_image.insertBefore(pO, output.group);
          planets.push(pO);
          var pB = document.createElementNS("http://www.w3.org/2000/svg", "use");
          pB.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#sec_planetB");
          svg_image.insertBefore(pB, sec_output.group);
          planets.push(pB);
          var pO = document.createElementNS("http://www.w3.org/2000/svg", "use");
          pO.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#sec_planetO");
          svg_image.insertBefore(pO, sec_output.group);
          planets.push(pO);
          updated = true;
        }
      }
      if (this_input != gear_idx) {
        this_input = gear_idx;
        if (this_input) {
          svg_image.removeChild(input.group);
          input.changeColor("rgba(160, 140, 0, 1.0)")
          svg_image.insertBefore(input.group, inputPointer.group);
        } else {
          svg_image.removeChild(input.group);
          input.changeColor("rgba(180, 0, 0, 1.0)")
          svg_image.insertBefore(input.group, base.group);
        }
      }
      while(planets.length > numPlanets*4) {
        svg_image.removeChild(planets.pop());
        updated = true;
      }

      base.changeTeeth(B, P, -1, size);
      output.changeTeeth(O, P, -1, size);
      planetB.changeTeeth(P, B, -1, size);
      planetO.changeTeeth(P, O, -1, size);
      sec_planetB.changeTeeth(sec_P, sec_B, -1, sec_size);
      sec_planetO.changeTeeth(sec_P, sec_O, -1, sec_size);
      input.changeTeeth(I, P, size + (this_input ? planetO.radius : planetB.radius), size);
      inputPointer.changeTeeth(I, P, size + (this_input ? planetO.radius : planetB.radius), size);
      sec_base.changeTeeth(sec_B, sec_P, -1, sec_size);
      sec_output.changeTeeth(sec_O, sec_P, -1, sec_size);
      sec_input.changeTeeth(sec_I, sec_P, sec_size + sec_planetO.radius, sec_size);
      if (base.updated || output.updated || input.updated || calc_from_scratch) updated = true;
      // if outputSpeed = 0  -> baseSpeed = carrierSpeed * (B-O)/B -> carrierSpeed = baseSpeed * B/(B-O)
      // if baseSpeed = 0 -> outputSpeed  = carrierSpeed * (O-B)/O -> carrierSpeed =  outputSpeed * O/(O-B)
      // (1) carrierSpeed = baseSpeed * B/(B-O) + outputSpeed * O/(O-B)
      //     Oolving (1) for baseSpeed:
      // (2) baseSpeed = (carrierSpeed - outputSpeed * O/(O-B)) * (B-O)/B
      //     Oolved (1) for outputSpeed:
      // (3) outputSpeed = (carrierSpeed - baseSpeed * B/(B-O)) * (O-B)/O
      sec_carrierSpeed = sec_I/(sec_B+sec_I) * sec_inputSpeed;

      // baseSpeed = (B == O) ? outputSpeed : (carrierSpeed - outputSpeed * O/(O-B)) * (B-O)/B;
      inputSpeed = sec_carrierSpeed * (sec_O-sec_B)/sec_O;
      carrierSpeed = I/(B+I) * inputSpeed;

      // baseSpeed = (B == O) ? outputSpeed : (carrierSpeed - outputSpeed * O/(O-B)) * (B-O)/B;
      outputSpeed = carrierSpeed * (O-B)/O;

      // Write calculated results to html
      if (!outputSpeed) return; // something has really gone very wrong if outputSpeed is zero here. But if so, return in order to avoid divide by zero further down
      sOp = 1;
      rOp = inputSpeed/outputSpeed;
      cOp = carrierSpeed/outputSpeed;
      testCRotations = 0;
      for (i=1; i<20; i++) {
        // how many rotations has the output done when the carrier has done 'i' rotations?
        testCRotations +=  cOp;
        deviation = testCRotations - testCRotations.toFixed(0);
        if (deviation < 0.001 && deviation > - 0.001) break;
      }
      carrierPeriod = 360 * testCRotations.toFixed(0);

      // find ideal planet positions
      // There are O+I places to choose from
      min_dist = 360 / (s_calc + I);
      ideal_dist = P & 1 ? 0 : min_dist / 2;
      fixed_offset = P & 1 ? 0 : 180/P;
      for (idx=0; idx < numPlanets; idx++) {
        while (ideal_dist < (360 * idx / numPlanets)) {
          ideal_dist += min_dist;
        }
        carrierAnglePos[idx] = ideal_dist;
        planetAngelOffset[idx] =  (O-B)*ideal_dist/P;
      }
      min_dist = 360 / (sec_O + sec_I);
      ideal_dist = sec_P & 1 ? 0 : min_dist / 2;
      sec_fixed_offset = sec_P & 1 ? 0 : 180/sec_P;
      for (idx=0; idx < numPlanets; idx++) {
        while (ideal_dist < (360 * idx / numPlanets)) {
          ideal_dist += min_dist;
        }
        sec_carrierAnglePos[idx] = ideal_dist;
        sec_planetAngelOffset[idx] =  (sec_O-sec_B)*ideal_dist/sec_P;
      }
    }

    low_lim = 200;

    today = new Date();
    today_sec = today/1000;
    today_sec -= today.getTimezoneOffset() * 60;
    secsRT = Math.floor(today_sec % (12*60*60));
    secsClock = outputAngle * (12*60*60/360);
    secsErr = secsRT - secsClock;  // positive err, speed up forwards to adjust to real time
    while (secsErr < -6*60*60) secsErr += 12*60*60;
    while (secsErr >  6*60*60) secsErr -= 12*60*60;
    if (secsErr > 60 || secsErr < -5) {
      calc_from_scratch = 1;
      updated = true;
    }
    if (!updated) {
      if (realTime && secsErr < 0 && secsErr > -2) return;
    }

    var inc = .007;
    if (calc_from_scratch) {
      inc = secsRT*360/(12*60*60)/outputSpeed;
      outputAngle = inc * outputSpeed;
      carrierAngle = inc * carrierSpeed;
      inputAngle = inc * inputSpeed;
      sec_carrierAngle = inc * sec_carrierSpeed;
      sec_inputAngle = inc * sec_inputSpeed;
      while (outputAngle >= 360) outputAngle -= 360;
      while (outputAngle < 0) outputAngle += 360;
      while (inputAngle >= 360) inputAngle -= 360;
      while (inputAngle < 0) inputAngle += 360;
      while (sec_inputAngle >= 360) sec_inputAngle -= 360;
      while (sec_inputAngle < 0) sec_inputAngle += 360;
    } else {
      outputAngle += inc * outputSpeed;
      carrierAngle += inc * carrierSpeed;
      inputAngle += inc * inputSpeed;
      sec_carrierAngle += inc * sec_carrierSpeed;
      sec_inputAngle += inc * sec_inputSpeed;
      if (outputAngle >= 360) outputAngle -= 360;
      else if (outputAngle < 0) outputAngle += 360;
      if (inputAngle >= 360) inputAngle -= 360;
      else if (inputAngle < 0) inputAngle += 360;
      if (sec_inputAngle >= 360) sec_inputAngle -= 360;
      else if (sec_inputAngle < 0) sec_inputAngle += 360;
    }
    if (carrierAngle >= carrierPeriod) carrierAngle -= carrierPeriod;
    else if (carrierAngle < 0) carrierAngle += carrierPeriod;

    output.group.setAttribute("transform", "rotate(" + outputAngle + ")");
    input.group.setAttribute("transform", "rotate(" + inputAngle + ")");
    inputPointer.group.setAttribute("transform", "rotate(" + inputAngle + ")");
    sec_output.group.setAttribute("transform", "rotate(" + inputAngle + ")");
    sec_input.group.setAttribute("transform", "rotate(" + sec_inputAngle + ")");
    for(var i=0; i < planets.length/4; ++i)
    {
      planetPosition = carrierAngle + carrierAnglePos[i];
      sec_planetPosition = sec_carrierAngle + sec_carrierAnglePos[i];
      planetAngle = gear_idx ? B/P * planetPosition + fixed_offset: I/P * (inputAngle - planetPosition);
      planets[i*4].setAttribute("transform", "rotate(" + planetPosition + ") translate(0," + Z + ") rotate(" + planetAngle + ")");
      planetAngle = gear_idx ? I/P * (inputAngle - planetPosition) : B/P * planetPosition + planetAngelOffset[i] + fixed_offset;
      planets[i*4+1].setAttribute("transform", "rotate(" + planetPosition + ") translate(0," + Z + ") rotate(" + planetAngle + ")");
      planetAngle = gear_idx ? sec_B/sec_P * sec_planetPosition + sec_fixed_offset: sec_I/sec_P * (sec_inputAngle - sec_planetPosition);
      planets[i*4+2].setAttribute("transform", "rotate(" + sec_planetPosition + ") translate(0," + sec_size + ") rotate(" + planetAngle + ")");
      planetAngle = gear_idx ? sec_I/sec_P * (sec_inputAngle - sec_planetPosition) : sec_B/sec_P * sec_planetPosition + sec_planetAngelOffset[i] + sec_fixed_offset;
      planets[i*4+3].setAttribute("transform", "rotate(" + sec_planetPosition + ") translate(0," + sec_size + ") rotate(" + planetAngle + ")");
    }
  });

  anim.start();
};
