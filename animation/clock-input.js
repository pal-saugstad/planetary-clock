
function changeInput(elem, inc) {
  var parent_id = elem.parentNode.id;
  var dest = parent_id + 'T';
  var curr_val = parseInt(document.getElementById(dest).innerHTML);
  var val = curr_val + inc;
  var lowest = 4;
  var highest = 200;
  var action = 1;
  switch (parent_id) {
    case "mult":
      lowest = 1;
      highest = 9;
    break;
    case "planets":
      lowest = 3;
      highest = 7;
    break;
  }
  if (val < lowest) val = lowest;
  if (val > highest) val = highest;
  if (val != curr_val) {
    document.getElementById(dest).innerHTML = val;
    document.getElementById("action").innerHTML = action;
  }
}

function newGear(elem, inc) {

  var outer_sel = [
    "248 224 217",
    "352 297 288",
    "438 365 354",
    "380 304 295",
    "324 243 236",
    "238 175 170",
    "105 72 70",
    "273 182 177",
    "365 225 219",
    "78 40 39"
  ];
  var inner_sel = [
    "85 80 68",
    "105 90 77",
    "42 35 30",
    "80 64 55",
    "93 72 62",
    "68 51 44",
    "65 45 39",
    "57 38 33",
    "105 63 55",
    "14 8 7"
  ];

  var parent_id = elem.parentNode.id;
  var sel = parent_id == 'inner' ? inner_sel : outer_sel;
  var dest = parent_id + 'T';
  var inputs = document.getElementById(dest).innerHTML.match(/\d+/g)
  var curr_val = parseInt(inputs[0]);
  var val = curr_val + inc;
  if (val < 1) val = 1;
  if (val > sel.length) val = sel.length;
  if (val != curr_val) {
    document.getElementById(dest).innerHTML = val + ": " + sel[val-1];
    document.getElementById("action").innerHTML = 1;
  }
}
