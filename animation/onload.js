


window.onload = function(){
  var B = 9, O = 10, I = 11;
  var inputSpeed = 0.06;
  var speed = 600;
  var freeze = false;
  var toothMult = 1;
  var tilt = 0;
  svg_side = 600;

  var rgbYellow = "rgba(180, 180, 0, 0.8)";
  var rgbDarkYellow = "rgba(90, 90, 0, 0.8)";
  var rgbRed = "rgba(180, 0, 0, 1.0)";
  var rgbDarkRed = "rgba(120, 0, 0, 1.0)";

  var numPlanets = 1;
  var P = 1;
  var zoom = 1;
  var Z = 1;

  var outputAngle = 0;
  var carrierAngle = 0;
  var inputAngle = 0;

  var base = new Gear(B, false, rgbRed, 1, "rgb(255, 0, 0)");
  var output = new Gear(O, false, rgbYellow, 1, "rgb(255, 255, 0)");
  var planetB = new Gear(P, false, rgbRed, 0);
  var planetO = new Gear(P, false, rgbYellow, 0);
  var input = new Gear(I, true, rgbRed, 1, "rgb(0, 0, 255)");
  //var inputPointer = new Gear(I, true, rgbYellow, 2, "rgb(0, 0, 255)");

  var this_approx = false;
  var this_input = 0;
  var inverted = true; // Input is ring if inverted is true
  var this_inverted = true;
  var sign_inverted = 1;
  var gear_idx = 0; // 0 -> Panetary gear is made with base (red) 1 -> Panetary gear is made with output (yellow)
  var anim = new Animation();

  var svg_image = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg_image.setAttribute("height", svg_side.toString());
  svg_image.setAttribute("width", svg_side.toString());
  svg_image.setAttribute("viewBox", -svg_side/2 + " " + -svg_side/2 + " " + svg_side + " " + svg_side);
  svg_image.setAttribute("preserveAspectRatio", "xMidYMid slice");

  var defs = document.createElementNS("http://www.w3.org/2000/svg", "defs");
  defs.appendChild(planetB.group);
  defs.appendChild(planetO.group);
  planetB.group.setAttribute("id", "planetB");
  planetO.group.setAttribute("id", "planetO");

  svg_image.appendChild(defs);
  svg_image.appendChild(input.group);
  svg_image.appendChild(base.group);
  svg_image.appendChild(output.group);
  //svg_image.appendChild(inputPointer.group);
  var planets = [];
  var carrierAnglePos = [];
  var planetAngelOffset = [];

  document.getElementById("preview").appendChild(svg_image);

  anim.setStage(function(){

    // Read input from html
    var calc_from_scratch = false;
    var action = parseInt(document.getElementById("action").innerHTML);
    updated = false;
    if (action != 0) {
      if (action & 1) calc_from_scratch = true;
      tilt = parseInt(document.getElementById("tiltT").innerHTML);
      freeze = !document.getElementById("speedStart").checked;
      speed = freeze ? 0 : parseInt(document.getElementById("speedT").innerHTML);
      zoom = parseInt(document.getElementById("zoomT").innerHTML);
      toothMult = parseInt(document.getElementById("multT").innerHTML);
      B = toothMult * parseInt(document.getElementById("baseT").innerHTML);
      O = toothMult * parseInt(document.getElementById("outputT").innerHTML);
      I = toothMult * parseInt(document.getElementById("inputT").innerHTML);
      inverted = I >= O;
      numPlanets = parseInt(document.getElementById("planetsT").innerHTML);
      sign_inverted = (I >= B) ? 1 : -1; // is +1 if inverted
      Pb = Math.floor(sign_inverted * (I-B)/2);
      sign_inverted = (I >= O) ? 1 : -1; // is +1 if inverted
      Ps = Math.floor(sign_inverted * (I-O)/2);
      P = Ps;
      s_calc = O;
      gear_idx = 1;
      if (document.getElementById("preferBase").checked && ((I != B) || (O == B))) {
        P = Pb;
        s_calc = B;
        gear_idx = 0;
      }
      inverted = I >= (gear_idx ? O : B);
      sign_inverted = inverted ? 1 : -1; // is +1 if inverted
      var approx = (I ^ (gear_idx ? O : B)) & 1;
      cannot = '';
      if (P == 0) {
        cannot = '<B title="It might help to increase \'Tooth multiplier\'"> Planet has no teeth</b>';
      } else if (!inverted && (P >= B || P >= O)) {
        P = 0;
        cannot = "Planet too big";
      }

      // We have teeth (I O B and P) and which gear shall be the planet gear (O or B to I) and if this is inverted
      // Calculate radii (rI, rO, rB) and for the planets (rPB, rPO)

      // Calclate rB and rPB relative to each other
      var rB = 1.0;
      var rPB = rB*P/B;
      Z = rB + sign_inverted * rPB; // Carrier radius

      // Calclate rO and rPO relative to each other
      var rO = 1.0;
      var rPO = rO*P/O;
      var fact = Z / (rO + sign_inverted * rPO); // factor to align both to same Carrier radius

      // align
      rO *= fact;
      rPO *= fact;

      // Calculate rI
      rI = Z + sign_inverted * (gear_idx == 1 ? rPO : rPB);

      // Find biggest
      biggest = rO > rB ? rO : rB;
      if (rI > biggest) biggest = rI;

      // Scale everything so it fits nicely in the animation window
      fact = 280.0 / biggest;

      rB *= fact;
      rO *= fact;
      rPB *= fact;
      rPO *= fact;
      Z *= fact;
      rI *= fact;

      svg_image.setAttribute("viewBox", -svg_side/2/zoom + " " + (-svg_side/2/zoom - tilt*svg_side/40) + " " + svg_side/zoom + " " + svg_side/zoom);
      document.getElementById("action").innerHTML = 0;

      if (planets.length < numPlanets*2) {
        while(planets.length < numPlanets*2) {
          var pB = document.createElementNS("http://www.w3.org/2000/svg", "use");
          pB.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#planetB");
          svg_image.insertBefore(pB, output.group);
          planets.push(pB);
          var pO = document.createElementNS("http://www.w3.org/2000/svg", "use");
          pO.setAttributeNS("http://www.w3.org/1999/xlink", "href", "#planetO");
          svg_image.insertBefore(pO, output.group);
          planets.push(pO);
          updated = true;
        }
      }
      if (this_input != gear_idx || this_approx != approx) {
        this_input = gear_idx;
        this_approx = approx;
        input.internal = this_inverted;
        if (this_input) {
          svg_image.removeChild(input.group);
          if (approx) input.changeColor(rgbDarkYellow);
          else input.changeColor(rgbYellow);
          svg_image.insertBefore(input.group, output.group);
        } else {
          svg_image.removeChild(input.group);
          if (approx) input.changeColor(rgbDarkRed);
          else input.changeColor(rgbRed);
          svg_image.insertBefore(input.group, base.group);
        }
        this_inverted = inverted;
      }
      while(planets.length > numPlanets*2) {
        svg_image.removeChild(planets.pop());
        updated = true;
      }
      planetB.changeTeeth(P, rPB, false);
      planetO.changeTeeth(P, rPO, false);
      base.changeTeeth(B, rB, !inverted);
      output.changeTeeth(O, rO, !inverted);
      input.changeTeeth(I, rI, inverted);
      //inputPointer.changeTeeth(I, rI, inverted);
      if (base.updated || output.updated || input.updated || calc_from_scratch) updated = true;
      /* Express i:c (i_c) and o:c (o_c) as A/B written as [A, B]
      i_c = [B+I, I] = [O * (B+I), O * I]
      o_c = [O-B, O] = [I * (O-B), I * O]
        Hence
        i_o = [O * (B+I), I * (O-B)]
      */
      // Hence
      carrierSpeed = inputSpeed * I / (B+I);
      outputSpeed  = inputSpeed * I * (O-B) / ((B+I) * O);

      // i_c_o contains the full whole number ratio info i:c:o seen from expressions above
      i_c_o = reduce([(B+I) * O, I * O , I * (O-B)]);

      // find ideal planet positions
      // There are O+I or B+I places to choose from
      pos_tot = s_calc + I;
      min_dist = 360 / pos_tot;
      pos_per_planet = pos_tot / numPlanets;
      offset_dist = P & 1 ? 0 : min_dist / 2;
      offset_B = P & 1 || !inverted ? 0 : 180/P;
      offset_I = P & 1 || inverted ? 0 : 180/P;
      for (idx=0; idx < numPlanets; idx++) {
        angle = Math.round(pos_per_planet * idx) * min_dist + offset_dist;
        carrierAnglePos[idx] = angle;
        planetAngelOffset[idx] =  (O-B)*angle/P;
      }
      document.getElementById("ratio").value = i_c_o[0]+" : "+i_c_o[1]+" : "+i_c_o[2];
      approx_i_c_o = (i_c_o[0]/i_c_o[2]).toFixed(2) + " : " + (i_c_o[1]/i_c_o[2]).toFixed(2) + " : 1";
      document.getElementById("approxRatio").value = approx_i_c_o;
      document.getElementById("outputSplitRingSun").innerHTML = "<b>Type: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</b>" + (cannot ? cannot : (inverted ? 'Split sun' : 'Split Ring'));
      if (gear_idx == 1) {
        plB = planetB.teeth;
        plO = "<b>" + planetB.teeth + "</b>";
      } else {
        plO = planetB.teeth;
        plB = "<b>" + planetB.teeth + "</b>";
      }
      document.getElementById("outputTable").innerHTML = " \
      <tr><td><b>Gear&nbsp;&nbsp;&nbsp;&nbsp;</b></td><td><b>&nbsp;&nbsp;&nbsp;&nbsp;Radius</b></td><td><b>&nbsp;&nbsp;&nbsp;&nbsp;Teeth</b></td><td align=\"right\"><b>Approx.<br>radius</b></td></tr> \
      <tr><td>Input</td><td align=\"right\">" + input.radius.toFixed(3) + "</td><td align=\"right\">" + input.teeth + "</td><td align=\"right\">" + (approx ? "Yes" : "") + "</td></tr> \
      <tr><td>Output</td><td align=\"right\">" + output.radius.toFixed(3) + "</td><td align=\"right\">" + output.teeth + "</td><td/></tr> \
      <tr><td>Base</td><td align=\"right\">" + base.radius.toFixed(3) + "</td><td align=\"right\">" + base.teeth + "</td><td/></tr> \
      <tr><td>Planet(B)</td><td align=\"right\">" + planetB.radius.toFixed(3) + "</td><td align=\"right\">" + plB + "</td><td/></tr> \
      <tr><td>Planet(O)</td><td align=\"right\">" + planetO.radius.toFixed(3) + "</td><td align=\"right\">" + plO + "</td><td/></tr> \
      <tr><td>Carrier</td><td align=\"right\">" + Z.toFixed(3) + "</td><td></td><td/></tr>";
    }

    if (!updated) {
      if (speed == 0) return;
    }

    inputAngle += speed * inputSpeed;
    mult_fact = inputAngle / inputSpeed;
    carrierAngle = carrierSpeed * mult_fact;
    outputAngle =  outputSpeed * mult_fact;

    output.group.setAttribute("transform", "rotate(" + outputAngle + ")");
    input.group.setAttribute("transform", "rotate(" + inputAngle + ")");
    //inputPointer.group.setAttribute("transform", "rotate(" + inputAngle + ")");
    for(var i=0; i < planets.length/2; ++i)
    {
      planetPosition = carrierAngle + carrierAnglePos[i];
      planetAngle = sign_inverted * (gear_idx ? B/P * planetPosition + offset_B: I/P * (inputAngle - planetPosition) + offset_I);
      planets[i*2].setAttribute("transform", "rotate(" + planetPosition + ") translate(0," + Z + ") rotate(" + planetAngle + ")");
      planetAngle = sign_inverted * (gear_idx ? I/P * (inputAngle - planetPosition) + offset_I : B/P * planetPosition + planetAngelOffset[i] + offset_B);
      planets[i*2+1].setAttribute("transform", "rotate(" + planetPosition + ") translate(0," + Z + ") rotate(" + planetAngle + ")");
    }
  });

  anim.start();
};
