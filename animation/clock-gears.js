/*
Gear image generation code adapted from http://jsbin.com/oresos/latest

Gear Animation code Copyright (c) 2014 Ryan Cahoon and is distributed under the following terms

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
  Software.
* The names of the contributors may not be used to endorse or promote products derived from this software without
  specific prior written permission.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// polar to cartesian
function polar(r,theta) { return [r*Math.sin(theta), r*Math.cos(theta)]; }

function make_gear(number_of_teeth, p, dist, internal, color, handPointer, handBrightness) {

  // Draw an involute gear in your browswer using JavaScript and SVG
  // Tested on Internet Explorer 10 and Firefox 22

  // Adapted from: Public Domain Parametric Involute Spur Gear by Leemon Baird, 2011, Leemon@Leemon.com http://www.thingiverse.com/thing:5505

  // see also http://grabcad.com/questions/tutorial-how-to-model-involute-gears-in-solidworks-and-show-design-intent

  // point on involute curve
  function q6(b,s,t,d) { return polar(d,s*(iang(b,d)+t)); }

  // unwind this many degrees to go from r1 to r2
  function iang(r1,r2) { return Math.sqrt((r2/r1)*(r2/r1) - 1) - Math.acos(r1/r2); }

  // radius a fraction f up the curved side of the tooth
  function q7(f,r,b,r2,t,s) { return q6(b,s,t,(1-f)*Math.max(b,r)+f*r2); }

  // rotate an array of 2d points
  function rotate ( points_array, angle ) {
    var answer =[];
    for(var i=0; i<points_array.length; i++) {
      var x=points_array[i][0];
      var y=points_array[i][1];
      var xr = x * Math.cos (angle) - y * Math.sin (angle);
      var yr = y * Math.cos (angle) + x * Math.sin (angle);
      answer.push( [xr,yr] );
    }
    return answer;
  }

  // involute gear maker
  function build_gear ( number_of_teeth, p, edge )
  {
    // number_of_teeth: Number of teeth of this gear
    // p: Pitch radius of this gear
    // dist: Axle distance between the gears
    var mm_per_tooth = p * 2 * pi / number_of_teeth;
    var c  = p + mm_per_tooth / 2 / pi;                    // radius of outer circle
    var b  = p * Math.cos(pressure_angle);             // radius of base circle
    var r  = 2*p-c;                                  // radius of root circle
    var t  = mm_per_tooth / 2;                         // tooth thickness at pitch circle
    var k  = -iang(b, p) - t/2/p;                      // angle where involute meets base circle on side of tooth

    // here is the magic - a set of [x,y] points to create a single gear tooth
    bottom_point = r<b ? k : -pi/number_of_teeth;
    var points=[ polar(r, bottom_point), // bottom
        q7(0,r,b,c,k, 1), // half way up
        q7(1,r,b,c,k, 1), // end of uphill
        q7(1,r,b,c,k,-1), // start of downhill
        q7(0,r,b,c,k,-1), // half way down
        polar(r, -bottom_point) ]; // bottom

    var answer = [];
    poly = number_of_teeth;
    if (edge == 0 && r > 14) {
      edge = 7;
      poly = 5;
    }
    // create every gear tooth by rotating the first tooth

    for (var i=0; i<number_of_teeth; i++ ) answer = answer.concat (  rotate( points, -i*2*pi/number_of_teeth ) );
    if (edge > 0 && number_of_teeth > 3) {
      answer = answer.concat (polar(r, bottom_point)); // very first point repeated to complete the gear
      points = [polar(edge, -pi/poly)];
      for (var i=0; i<poly*4+1; i++ ) answer = answer.concat (  rotate( points, i*pi/2/poly ) );
    }
    return answer; // returns an array of [x,y] points
  }

  function mirror ( coords ) {
    return [-coords[0], coords[1]];
  }

  function build_hand ( hand_length )
  {
    var widen = (350-hand_length)/30;
    var hand_width = 6+widen;
    var hand_center = -50+widen;
    var hand_pointer = -hand_length - 8;
    var hand_anchor_inner = -hand_length + 8;
    if (hand_pointer > hand_center) {
      hand_center -= 100;
      hand_anchor_inner = -hand_length - 8
    }
    var answer = [
         [0, hand_pointer],
         [hand_width/2+4, -hand_length-2],
         [hand_width/2+4, -hand_length],
         [hand_width/2, hand_anchor_inner],
         [hand_width/2, hand_center]];
    answer_length = answer.length;
    for (var i=answer_length; i > 1;) {
      i--;
      answer = answer.concat ( mirror(answer[i]) );
    }

    return answer; // returns an array of [x,y] points
  }

  function hand_color ( handBrightness )
  {
    greyness = handBrightness;
    return "rgba("+greyness+","+greyness+","+greyness+", 1.0)";
  }

  // create polygon using pointlist
  var edge = input_thickness;
  var edge = p+(internal ? edge : -edge);
  if (handPointer < 2) {
    var gear1 = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    gear1.setAttribute("points", build_gear(number_of_teeth, p, edge).toString());
  }

  // add the new graphics to the document structure

  var group = document.createElementNS("http://www.w3.org/2000/svg", "g");
  if (handPointer < 2) {
    gear1.setAttribute("fill", color);
    group.appendChild(gear1);
  }
  if (handPointer >= 1) {
    var hand = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    hand.setAttribute("points", build_hand((p+edge)/2).toString());
    hand.setAttribute("fill", hand_color(handBrightness).toString());
    group.appendChild(hand);
  }

  group.changeTeeth = function(new_teeth, new_p, size, handPointer) {
      edge = input_thickness;
      edge = new_p+(internal ? edge : -edge);
      if (handPointer < 2) {
        gear1.setAttribute("points", build_gear(new_teeth, new_p, edge).toString());
      }
      if (handPointer >= 1) {
        hand.setAttribute("points", build_hand((new_p+edge)/2).toString());
      }
  }

  group.changeColor = function(color) {
      gear1.setAttribute("fill", color);
  }

  return group;
}

class Gear {
    // number_of_teeth: Number of teeth of this gear
    constructor (number_of_teeth, conn_teeth, dist, internal, color, handPointer, handBrightness=0) {
    // conn_teeth: Number of teeth of the connecting gear
    // dist: distance between axles of the gears
    // handPointer: 0: no handPointer, 1: as an additional element, 2: Just handPointer, no gear
    this.internal = internal;
    this.radius = this.calc_r(number_of_teeth, conn_teeth, dist);
    this.teeth = number_of_teeth;
    this.dist = dist;
    this.color = color;
    this.updated = false;
    this.handPointer = handPointer;
    this.handBrightness = handBrightness;
    this.group = make_gear(this.teeth, this.radius, dist, internal, color, handPointer, handBrightness);
  }

  calc_r(calc_teeth, conn_teeth, dist) {
    if (this.internal) {
      var divisor = calc_teeth - conn_teeth;
      if (divisor > 0) {
        return dist * calc_teeth / divisor;
      } else if (divisor < 0) {
        return dist * calc_teeth / -divisor;
      }
    } else {
      return dist * calc_teeth / ( calc_teeth + conn_teeth );
    }
    return 1;
  }

  changeTeeth(number_of_teeth, conn_teeth, radius, dist) {
    var r = radius > 0 ? radius : this.calc_r(number_of_teeth, conn_teeth, dist);            // radius of pitch circle
    this.dist = dist;
    if (number_of_teeth != this.teeth || r != this.radius) {
      this.group.changeTeeth(number_of_teeth, r, dist, this.handPointer);
      this.radius = r;
      this.teeth = number_of_teeth;
      this.updated = true;
    } else {
      this.updated = false;
    }
  }
  changeColor(color) {
    this.color = color;
    this.group.changeColor(color);
  }
}
